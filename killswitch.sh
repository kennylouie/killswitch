#!/bin/bash

set -e
set -u

sleep 60

ufw --force enable
ufw default deny outgoing
ufw default deny incoming

ufw allow out on lo from any to any
ufw allow in on lo from any to any

ufw allow in to 10.0.2.15/24
ufw allow out to 10.0.2.15/24

ufw allow out on surfshark_wg from any to any
ufw allow in on surfshark_wg from any to any

ufw allow out on surfshark_ipv6 from any to any
ufw allow in on surfshark_ipv6 from any to any

ufw allow out on docker0 from 172.17.0.0/16

ufw status
