#!/bin/bash

set -e
set -u

ufw --force disable
ufw --force reset
