# Linux VPN Killswitch

## Introduction

Simple UFW based killswitch for surfshark VPN.

## Usage

1. Use the install.sh script to install files
```
./install.sh
```
2. Ensure the VPN is on first as the UFW rules will block starting a new VPN connection. This can be circumvented by adding a line to allow connection to a specific surfshark location. The exact IP needs to be added.
3. Start the service.
```
sudo systemctl start killswitch
```
4. The sleep is there in conjunction with an auto startup with surfshark and auto connect. The service will wait 30 seconds before enabling ufw.

## Uninstall

1. Simply delete the scripts and service that were added by the install script in their respective locations.
