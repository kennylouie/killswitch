#!/bin/bash

set -e
set -u

sudo cp killswitch.service /etc/systemd/system/.
sudo cp killswitch.sh /usr/local/bin/.
sudo cp restore-ufw.sh /usr/local/bin.

sudo chmod 500 /usr/local/bin/killswitch.sh
sudo chmod 500 /usr/local/bin/restore-ufw.sh

sudo chown root:root /usr/local/bin/killswitch.sh
sudo chown root:root /usr/local/bin/restore-ufw.sh
